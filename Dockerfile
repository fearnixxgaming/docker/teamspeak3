FROM ubuntu:18.04

MAINTAINER FearNixx Technik, <technik@fearnixx.de>

# System locale
#RUN locale-gen en_US.UTF-8  
ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US:en  
ENV LC_ALL en_US.UTF-8  
# /System locale

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get -y install curl ca-certificates openssl libssl-dev git tar sqlite fontconfig iproute2 gcc gcc-5 gcc-6 gcc-7 gcc-8 libgcc-5-dev libgcc-6-dev libgcc-7-dev libgcc1 && \
    apt-get clean

RUN useradd -m -d /home/container -s /bin/bash container

USER container
ENV  USER=container HOME=/home/

WORKDIR /home/container

COPY ./entrypoint.sh /entrypoint.sh

CMD ["/bin/bash", "/entrypoint.sh"]
